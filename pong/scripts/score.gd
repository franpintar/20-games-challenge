extends Control

@export var player_1_score_label: Label
@export var player_2_score_label: Label


func update(player_1_score: int, player_2_score: int):
	player_1_score_label.text = str(player_1_score)
	player_2_score_label.text = str(player_2_score)

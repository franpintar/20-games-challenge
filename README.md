## About

Personal submission for the 20 Games Challenge. The full challenge can be found here: https://20_games_challenge.gitlab.io/challenge/


## License

[MIT](https://choosealicense.com/licenses/mit/)
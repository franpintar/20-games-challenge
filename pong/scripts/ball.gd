extends CharacterBody2D


const BALL_SPEED = 500.0
var direction = Vector2()

signal collided(collider)


func _physics_process(delta):
	velocity = BALL_SPEED * direction * delta
	var collision = move_and_collide(velocity)
	if collision:
		emit_signal("collided", collision.get_collider())
		direction = direction.bounce(collision.get_normal())

func reset():
	var new_direction = Vector2()
	
	position.x = 576 # TODO replace by var 
	position.y = 324 # storing the screen center
	new_direction.x = [-1, 1].pick_random()
	new_direction.y = randf_range(-1, 1)
	direction = new_direction.normalized()

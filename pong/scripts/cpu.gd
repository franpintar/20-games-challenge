extends StaticBody2D

@export var ball: CharacterBody2D

const CPU_SPEED := 10

var direction := 0
var distance_to_ball := 0.0


func _process(delta):
	position.y = clamp(position.y + distance_to_ball * CPU_SPEED * delta,
					   0 + Game.PLAYER_HEIGHT/2, # Top boundary
					   648 - Game.PLAYER_HEIGHT/2) # Bottom boundary

func get_direction():
	distance_to_ball = ball.position.y - position.y


func _on_timer_timeout():
	get_direction()

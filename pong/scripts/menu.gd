extends Control

@export var main_menu: VBoxContainer
@export var options_menu: VBoxContainer

@onready var menus = [main_menu,
					  options_menu,
					 ]

signal pause_button_pressed


func _ready():
	hide()


func _input(event):
	if event.is_action_pressed("escape"):
		emit_signal("pause_button_pressed")


func toggle():
	if !visible: # If not already on main menu
		self.show()
		go_to_menu(main_menu)
	else:
		self.hide()


func go_to_menu(target):
	for menu in menus:
		if menu == target:
			menu.show()
			menu.get_child(0).grab_focus() # Focus on the first menu option to facilitate navigation
		else:
			menu.hide()


func _on_options_button_pressed():
	go_to_menu(options_menu)


func _on_back_button_pressed():
	go_to_menu(main_menu)

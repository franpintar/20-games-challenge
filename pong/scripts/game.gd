class_name Game
extends Node2D

const PLAYER_SPEED := 500.0
const PLAYER_HEIGHT := 100.0

@export var ball: CharacterBody2D
@export var boundaries: StaticBody2D
@export var menu: Node
@export var audio: Node
@export var players: Array[StaticBody2D]
@export var score: Control


enum PlayMode {SINGLE_PLAYER, TWO_PLAYERS}

static var current_play_mode: PlayMode

var player_1_score := 0
var player_2_score := 0


func _ready():
	current_play_mode = PlayMode.TWO_PLAYERS
	menu.connect("pause_button_pressed", _on_pause_button_pressed)
	ball.reset()


func _process(delta):
	var direction_player_1 = Input.get_axis("move_up_player_1", "move_down_player_1")
	if direction_player_1:
		players[0].position.y = clamp(players[0].position.y + direction_player_1 * PLAYER_SPEED * delta, 0 + PLAYER_HEIGHT/2, 648 - PLAYER_HEIGHT/2)
	if current_play_mode == PlayMode.TWO_PLAYERS:
		var direction_player_2 = Input.get_axis("move_up_player_2", "move_down_player_2")
		if direction_player_2:
			players[1].position.y = clamp(players[1].position.y + direction_player_2 * PLAYER_SPEED * delta, 0 + PLAYER_HEIGHT/2, 648 - PLAYER_HEIGHT/2)
	


func goal(scorer):
	if scorer == 1:
		player_1_score += 1
	if scorer == 2:
		player_2_score += 1
	score.update(player_1_score, player_2_score)
	audio.play_sound(audio.LONG_BEEP)
	await get_tree().create_timer(1.0).timeout
	ball.reset()


func restart():
	audio.restart_music()
	player_1_score = 0
	player_2_score = 0
	score.update(player_1_score, player_2_score) # Reset score labels
	ball.reset()
	pause_unpause()


func pause_unpause():
		get_tree().paused = !get_tree().paused
		menu.toggle()


func _on_goalpost_left_body_entered(body):
	if body == ball:
		goal(2)


func _on_goalpost_right_body_entered(body):
	if body == ball:
		goal(1)


func _on_pause_button_pressed():
	pause_unpause()


func _on_exit_button_pressed():
	get_tree().quit(0)


func _on_continue_button_pressed():
	pause_unpause()


func _on_restart_button_pressed():
	restart()


func _on_ball_collided(collider):
	if collider in players:
		audio.play_sound(audio.BEEP)
		#TODO: Speed up every time a player hits the ball
	elif collider == boundaries:
		audio.play_sound(audio.PLOP)


func _on_music_button_toggled(_toggled_on):
	audio.toggle_music()


func _on_sfx_button_toggled(_toggled_on):
	audio.toggle_sfx()

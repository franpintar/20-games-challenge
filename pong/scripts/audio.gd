extends Node

enum {BEEP, LONG_BEEP, PLOP}

@export var music: AudioStreamPlayer
@export var sfx: AudioStreamPlayer
@export var beep: AudioStream
@export var long_beep: AudioStream
@export var plop: AudioStream

var sfx_on := true


func toggle_music():
	music.stream_paused = !music.stream_paused


func toggle_sfx():
	sfx_on = !sfx_on


func restart_music():
	music.play()


func play_sound(sound):
	if sfx_on:
		match sound:
			BEEP:
				sfx.stream = beep
			LONG_BEEP:
				sfx.stream = long_beep
			PLOP:
				sfx.stream = plop
		sfx.play()
